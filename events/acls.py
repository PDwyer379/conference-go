import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json


def get_photo(city, state):
    params = {"per_page": 1, "query": f"{city} {state}"}
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    # url_link = content["photos"]["url"]
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except {KeyError, IndexError}:
        return {"picture_url": None}


def get_weather(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    location_url = "http://api.openweathermap.org/geo/1.0/direct?"
    response = requests.get(location_url, params=params)
    lat_lon = json.loads(response.content)
    lat = lat_lon[0]["lat"]
    lon = lat_lon[0]["lon"]
    print(lat_lon)

    weather_param = {"lat": lat, "lon": lon, "appid": OPEN_WEATHER_API_KEY}
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    w_response = requests.get(weather_url, weather_param)
    weather_data = json.loads(w_response.content)
    temperature = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    try:
        return {"temperature": temperature, "description": description}
    except {KeyError, IndexError}:
        return {"temperature": None, "description": None}
